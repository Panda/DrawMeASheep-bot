waitRules = 10
waitDrawing = 90
waitGuess = 10
wantToDraw = "dessiner"
wantToChoose = "choisir"

userDirectory = "data/userInfos/"


hashtag = "DrawMeASheep"

helpMsg = "Si tu veux que je te demande de dessiner quelque chose pour moi, envoie en message direct \""+str(wantToDraw)+"\" ou \"" +wantToChoose + "\" + mot pour dessiner le  mot choisi."
rules = "Dans " + str(waitRules) + """ secondes, je vais te donner un mot que tu devras dessiner et m'envoyer en réponse au prochain toot.

Tu auras seulement """ + str(waitDrawing) + """ secondes pour le dessiner et me l'envoyer, ça va très vite !

Après cela, je demanderai à Mastodon de deviner ce qu'était ton dessin !

Conseil : ouvre maintenant un logiciel de dessin (Paint, Gimp...) et une fois que tu as dessiné, copie le dessin et colle-le directement dans Mastodon, dans ton toot."""
preWord = "Le mot que je voudrais que tu dessines est : \""
postWord = "\" \n\n Donne ton dessin en réponse à ce toot s'il te plait. Tu as seulement " + str(waitDrawing) + " secondes !  Vite, vite !"

dictionnary = "data/dictionnaryM.txt"
dictionnaryHard = "data/dictionnaryH.txt"
sorryTooLateDraw = "Mince, il ne reste plus de temps ! C'est très rapide je sais... Mais je sais aussi que la prochaine fois tu y arriveras !!"

preUser = """Hey Mastodon ! J'ai demandé à @"""
postUser = """ de dessiner quelque chose pour moi. Je te donne """ + str(waitGuess) + """ minutes pour deviner ce que c'est.

Si tu es le ou la première à trouver, tu as gagné !

(boosts appréciés)

#""" + hashtag

appName = "DrawMeASheepApp"




appCred = "data/appCred.dat"
accountCred = "data/accountCred.dat"
apiBaseURLFile = "data/apiBaseURL.dat"
botNameFile = "data/botName.dat"
emergencyContactFile = "data/emergencyContact.dat"
proposeEmergencyContact = True
emergencyMessage = "Hey !\n J'ai rencontré une erreur récemment... Voilà, pour te mettre au courant... Bon courage !"
verboseBase = 10
verboseError = 10

debug = True

nameMaxLength = 32
tootLength = 500

log = "data/log/log.txt"

sinceID = "data/log/since_id.txt"

botName = "DrawMeASheep"
