import random
import mastodon
import Consts
import  pickle
from TootHTMLParser import TootHTMLParser
import threading
import Log
import os
from collections import deque

#TODO : lock before and after modifying waitIDs & guessIDs


#a Mastodon Stream Listener  defines functions to react when something happens on Mastodon. We inherit it.
class BotListener(mastodon.StreamListener):
    def __init__(self, bot):
        self.bot = bot
        self.guessIDs = {}
        self.waitIDs = {}
        self.alreadyGuessedIDs = deque(maxlen = 15)
        self.threads = []

    #What happens if the bot receives a notification ? We only want to react to DM.
    def on_notification(self, notification):
        for thread in self.threads:
            if(not thread.is_alive()):
                Log.logprint("removing thread" + str(thread))
                self.threads.remove(thread)


        #We react to mentions only
        if(notification['type'] != 'mention'):
            Log.logprint("nevermind, it's not a mention")
            return

        #So there is a toot !
        status = notification['status']


        #And there is a text in this toot. But it is mixed with HTML we don't want so we get rid of it.
        content = str(status["content"])
        parser = TootHTMLParser()
        parser.feed(content)
        content = (parser.txt).lower()

        Log.logprint(content)


        atUser = status["account"]["acct"]
        userInfo = self.associateToUser(atUser)

        #If the toot is not in answer to a drawing
        answerTo = status["in_reply_to_id"]
        index = self.inqueue(answerTo)
        if(index >= 0):
            word = self.alreadyGuessedIDs[index][1]["word"]
            user = self.alreadyGuessedIDs[index][1]["user"]
            guessedBy = self.alreadyGuessedIDs[index][1]["guessedBy"]
            if((" " + word)  in content):
                self.bot.toot("Bravo, tu as trouvé que le dessin de " + user + " correspond au mot \"" + word + "\". \n\n Malheureusement " + guessedBy + " a trouvé avant toi !" , status, True)
            else:
                self.bot.toot("Non, ce n'est pas la réponse que j'attends !" , status, True)
        else:
            if(answerTo not in self.guessIDs):
                if(answerTo not in self.waitIDs):
                    if((Consts.wantToChoose+" ") in content):
                        if(status["visibility"] == "direct"):
                            Log.logprint("I'm treating a notification... a DM to choose what to draw not answer or ask guess")
                            dictionnary = Consts.dictionnary
                            toot = self.bot.toot(Consts.rules, status, True)
                            wordToFind = ((content.split(" "))[-2]).lower()
                            t = threading.Timer(Consts.waitRules, self.giveWord, [wordToFind, toot, atUser])
                            t.start()
                            self.threads.append(t)
                    elif(not (" "+Consts.wantToDraw+" ") in content):
                        if(answerTo == None):
                            Log.logprint("I'm treating a notification... Not an answer to a ask or guess, not draw ; and new")
                            self.bot.toot(Consts.helpMsg, status, True)
                        else:
                            Log.logprint("I'm treating a notification... Not an answer to a ask or guess not draw but not new")
                    else: #I want to draw please please pleeaaaase !
                        if(status["visibility"] == "direct"):
                            Log.logprint("I'm treating a notification... a DM to draw not it answer of askguess")
                            dictionnary = Consts.dictionnary
                            toot = self.bot.toot(Consts.rules, status, True)
                            wordToFind = "sheep"
                            with open(dictionnary) as dict:
                                words = dict.readlines()
                                wordToFind = words[random.randrange(len(words))].strip()
                            t = threading.Timer(Consts.waitRules, self.giveWord, [wordToFind, toot, atUser])
                            t.start()
                            self.threads.append(t)
                        else:
                            Log.logprint("I'm treating a notification... not a DM, to draw, not in answer of askguess")
                else: #I try to give my drawing
                    if(status["visibility"] != "direct"):
                        Log.logprint("I'm treating a notification... answer but not in DM")
                        self.bot.toot("Ta réponse devrait être en message direct (DM). Je ne peux pas la prendre en compte.", status, True)
                    elif(len(status["media_attachments"]) != 1):
                        Log.logprint("I'm treating a notification... answer DM but not 1 media")
                        self.bot.toot("Ta réponse devrait être un (1) dessin.", status, True)
                    else: #answer is valid
                        Log.logprint("I'm treating a notification... Accepting an answer")
                        word = self.waitIDs[answerTo]["word"]
                        user = self.waitIDs[answerTo]["user"]
                        if(user == status["account"]["acct"]):
                            del self.waitIDs[answerTo]
                            media = status["media_attachments"][0]
                            user = status["account"]["acct"]
                            toot = self.bot.toot(Consts.preUser + user + Consts.postUser, None, False, media)
                            self.guessIDs[toot["id"]] = {"word" : word, "user" : user}
                            t = threading.Timer(Consts.waitGuess*60, self.stopGuess, [toot, word, user])
                            t.start()
                            self.threads.append(t)
                            userInfo["drawings"] += 1
                            self.saveUserInfo(atUser, userInfo)
                        else:
                            Log.logprint("There is something WRONG", Consts.verboseError)
            else: #I try to guess the drawing
                word = self.guessIDs[answerTo]["word"]
                user = self.guessIDs[answerTo]["user"]
                if(user == status["account"]["acct"]): # the person who try to guess is the drawer...
                    self.bot.toot("Hé, j'espère que tu n'es pas en train d'essayer de tricher, hihi ;-)", status, False)
                    Log.logprint("I'm treating a notification... drawer \"guess\"?")
                else:
                    Log.logprint("----"+word+"----")
                    if((" " + word)  in content):
                        Log.logprint("I'm treating a notification... somebody guessed !")

                        self.alreadyGuessedIDs.append([answerTo , {"word":word, "user":user, "guessedBy":atUser}])

                        self.bot.toot("Bravo ! En effet, la réponse était \"" + word + "\". Merci @" + user + " pour son talent en dessin ! \n\n #"+Consts.hashtag , status, False)
                        #media = self.bot.apiMasto.status(answerTo)["media_attachments"][0]
                        #self.bot.toot("Bravo à @" + atUser + " qui a trouvé que le dessin de @" + user + " correspond au mot \"" + word + "\". \n\n J'espère que vous vous êtes amusé·es ! \n\n" + Consts.helpMsg + "\n\n#" + Consts.hashtag , None, False, media)
                        del self.guessIDs[answerTo]

                        userInfo["guesses"] += 1
                        self.bot.toot("@" + atUser + " J'ai mis à jour tes points ! \n\n Dessins : " + str(userInfo["drawings"]) + "\n Dessins devinés par les autres : " + str(userInfo["drawingsGuessed"]) + "\n Dessins que tu as deviné : " + str(userInfo["guesses"]) , None, True)
                        self.saveUserInfo(atUser, userInfo)
                        userInfo = self.associateToUser(user)
                        userInfo["drawingsGuessed"] += 1
                        self.bot.toot("@" + user + " J'ai mis à jour tes points ! \n\n Dessins : " + str(userInfo["drawings"]) + "\n Dessins devinés par les autres : " + str(userInfo["drawingsGuessed"]) + "\n Dessins que tu as deviné : " + str(userInfo["guesses"]) , None, True)
                        self.saveUserInfo(user, userInfo)


                    else:
                        self.bot.toot("Non, ce n'est pas la réponse que j'attends !" , status, True)


        #We update the new last ID treated.
        self.bot.sinceID = notification["id"]
        open(Consts.sinceID, 'w').write(str(self.bot.sinceID))


    def giveWord(self, word, status, atUser):
        user = status["account"]["acct"]
        toot = self.bot.toot("@" + atUser + " " + Consts.preWord + word + Consts.postWord, status, True)
        self.waitIDs[toot["id"]] = {"word" : word, "user" : atUser}
        t = threading.Timer(Consts.waitDrawing, self.endOfTimer, [toot, atUser])
        t.start()
        self.threads.append(t)

    def endOfTimer(self, toot, atUser):
        if(toot["id"] in self.waitIDs): #Too Late ! The ID has not been removed from the lists. That means we don't have a drawing.
            self.bot.toot("@" + atUser + " " + Consts.sorryTooLateDraw, toot, True)
            del self.waitIDs[toot["id"]]

    def stopGuess(self, toot, word, user):
        if(toot["id"] in self.guessIDs): #Too late. I give the answer.
            self.bot.toot("Personne n'a trouvé ce que @" + user + " avait dessiné pour moi... \n \n Le mot à deviner était \"" + word + "\". Ce n'est pas facile mais vous trouverez la prochaine fois ! \n\n#" + Consts.hashtag,toot, False)
            del self.guessIDs[toot["id"]]

    def associateToUser(self, user):
        filename = Consts.userDirectory + user + ".dat"
        if(os.path.exists(filename)):
            with open(filename, 'rb') as input:
                return pickle.load(input)
        else:
            return {"drawings" : 0, "drawingsGuessed" : 0, "guesses" : 0}

    def saveUserInfo(self, user, userInfo):
        filename = Consts.userDirectory + user + ".dat"
        with open(filename, 'wb') as output:  # Overwrites any existing file.
            pickle.dump(userInfo, output, pickle.HIGHEST_PROTOCOL)

    def inqueue(self, id):
        index = 0
        for array in self.alreadyGuessedIDs:
            if(array[0] == id):
                return index
            index += 1
        return -1
