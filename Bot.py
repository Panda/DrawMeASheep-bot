import mastodon
import os
import time
import mimetypes
from urllib.request import urlretrieve
from PIL import Image

from TootHTMLParser import TootHTMLParser
import Consts
from BotListener import BotListener
import Log



#The bot that rocks. Can listen to events and publish toots !
class Bot(object):
    def __init__(self):
        #First we can connect using the api.
        api_base_url = open(Consts.apiBaseURLFile).read()
        self.apiMasto = mastodon.Mastodon(client_id = Consts.appCred, access_token = Consts.accountCred,api_base_url = api_base_url)
        Log.logprint("Bot connected.")

        #We'll treat every message since the last one we treated.
        if os.path.exists(Consts.sinceID) and open(Consts.sinceID).read().isdigit():
            self.sinceID = int(open(Consts.sinceID).read())
        else:
            self.sinceID = 0

        self.listener = BotListener(self)


    def toot(self, text, status, isDirect = True, media = None):
        atUser = ""
        if(status is not None and status["account"]["acct"] != Consts.botName):
            atUser =  '@' + status['account']['acct']
            text = atUser + " " + text

        if(isDirect):
            visibility = 'direct'
        else:
            visibility = 'public'


        if(media is not None):
            if(media["type"] == "image"):
                image = Image.open(urlretrieve(media["url"])[0])
                if image.mode == 'CMYK':
                    image = image.convert('RGB')
                image.save("data/media_temp.png")
                media = self.apiMasto.media_post("data/media_temp.png")
                media = [media["id"]]

            else:
                media = None


        reply = None
        # while(len(text) > Consts.tootLength):
        #     if(status is not None):
        #         reply = status['id']
        #     status = self.apiMasto.status_post(text[:Consts.tootLength-1], in_reply_to_id=reply, visibility=visibility, media_ids = media)
        #     text = atUser + ' ' + text[Consts.tootLength-1:]

        if(status is not None):
            reply = status['id']
        status = self.apiMasto.status_post(text[:Consts.tootLength-1], in_reply_to_id=reply, visibility=visibility, media_ids=media, sensitive = True)
        return status

    #We just stream the notifications. Never ending function (in theory)
    def listen(self):
        Log.logprint("Listening for events since last time (ID " + str(self.sinceID) + ")")
        for notification in reversed(self.apiMasto.notifications(since_id=self.sinceID)):
            self.listener.on_notification(notification)


        Log.logprint("Now listening for new events...")
        self.apiMasto.stream_user(self.listener)

    def emergencyContact(self):
        contact = open(Consts.emergencyContactFile).read()
        self.apiMasto.status_post('@' + contact + " " + Consts.emergencyMessage, visibility = 'direct')
