import Consts
import time

def logprint(text, verbose = Consts.verboseBase):
    text = str(time.asctime( time.localtime(time.time()) )) + " : " + str(text) + "\n\n"
    if(verbose >= Consts.verboseError):
        with open(Consts.log, 'a') as output:
            output.write(text)
    if(Consts.debug):
        Log.logprint(text)
